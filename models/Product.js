/* -- SETUP -- */
const mongoose = require("mongoose");

/* -- MODEL -- */
const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is Required!"]
	},
	price: {
		type: Number,
		required: [true, "Product Price is Required!"]
	},
	description: {
		type: String,
		required: [true, "Product Description is Required!"]
	},
	isAvailable: {
		type: Boolean,
		default: true
	}
}, {timestamps: true});

/* -- EXPORT ACCESS -- */
// Collection
module.exports = mongoose.model("Products", productSchema);