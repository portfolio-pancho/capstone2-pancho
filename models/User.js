/* -- SETUP -- */
const mongoose = require("mongoose");

/* -- MODEL -- */
const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "User Email is Required."]
	},
	password: {
		type: String,
		required: [true, "User Password is Required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
});

/* -- EXPORT ACCESS -- */
// Collection
module.exports = mongoose.model("Users", userSchema);