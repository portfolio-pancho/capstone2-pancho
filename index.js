/* -- SETUP MODULES / DEPENDENCIES -- */
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute.js"); 
const productRoute = require("./routes/productRoute.js"); 

/* -- SERVER SETUP -- */
const app = express();
const port = process.env.PORT || 8000;

app.listen(port, () => {
	console.log(`API is online on port ${port}`);
});

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// --------------------------------------------------- //
// Database Connection (MongoDB)
mongoose.set('strictQuery', true);
mongoose.connect("mongodb+srv://ashley:0000@cluster2.q1pbdza.mongodb.net/?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology:true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "CONNECTION ERR0R!"));
db.once("open", () => console.log("The Waffle House Has Found It's New Host!"));

// Routes
app.use("/user", userRoute);
app.use("/product", productRoute);