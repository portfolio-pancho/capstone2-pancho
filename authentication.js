/* -- SETUP -- */
const jwt = require("jsonwebtoken");

/* -- TOKEN CREATION -- */
const secret = "secretKeyword";
module.exports.createToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {});
}

/* -- TOKEN VERIFICATION -- */
module.exports.verifyToken = (req, res, next) => {
	let token = req.headers.authorization;
	// console.log(token);
	if(typeof token !== 'undefined') {
		// bearer encode something
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				// failed
				return 'Error occurred - decoding failed.';
			} else {
				// successful
				next();
			}
		});
	} else {
		return `Failed - token does not exist.`
	}
}

/* -- DECODE TOKEN -- */
module.exports.decodeToken = (token) => {
	if(typeof token !== 'undefined') {
		// bearer decode something
		token = token.slice(7, token.lengths)
		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				// failed
				return 'Error occurred - decoding failed';
			} else {
				// successful
				const payload = jwt.decode(token, {complete: true}).payload
				if(payload.isAdmin){
					return payload;
				}else{
					return 'Error occurred - operation failed';
				}
			}
		})
	} else {
		return 'Error occurred - token does not exist.';
	}
}
