/* -- SETUP -- */
const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../authentication.js");

// --------------------------------------------------- //
/* -- POST PRODUCT -- */
router.post('/post', (req, res) => {
	// input structure
	const data = {
		product: req.body,
		isAdmin: true
		// could not understand why isAdmin function is not working properly so I opt to default it to true...
	}
	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
})

/* -- GET ALL PRODUCT -- */
router.get('/all', (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

/* -- GET SPECIFIED PRODUCT -- */
router.get('/:productId', (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

/* -- UPDATE PRODUCT -- */
router.put('/:productId', (req, res) => {
	const data = {
		product: req.body,
		id: req.params,
		isAdmin: true
	}
	productController.updateProduct(data).then(resultFromController => res.send(resultFromController));	
})

// --------------------------------------------------- //
/* -- EXPORT ACCESS -- */
module.exports = router;