/* -- SETUP -- */
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../authentication.js");

// --------------------------------------------------- //
/* -- REGISTER USER -- */
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

/* -- LOGIN USER -- */
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

/* -- RETRIEVE USER DETAILS -- */
router.get('/:userId/details', auth.verifyToken, (req, res) => {
	let data = {
		userId: req.params.userId,
		userToken: auth.decodeToken(req.headers.authorization).id
	}
	userController.getUser(data).then(resultFromController => res.send(resultFromController));
})

/* -- SET USER TO ADMIN -- */
// couldn't get it to work because of low understanding (my own fault) and time crunch.

// --------------------------------------------------- //
/* -- EXPORT ACCESS -- */
module.exports = router;