/* -- SETUP -- */
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../authentication.js");

// --------------------------------------------------- //
/* -- USER REGISTRATION -- */
module.exports.registerUser = (reqBody, res) => {
    return User.find({ email: reqBody.email }).then(result => {
        if(result.length > 0) {
            // checks if email already registered 
            return 'Email address entered already registered. Use a different email or sign in with an existing account.';
        } else {
            // register new email
            let newUser = new User({
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10) 
            });
            // stores new email as an object
            return newUser.save().then((user, error) => {
                if(error) {
                    return 'Error occurred - Email address failed to register';
                } else {
                    return `Email address successfully registered and now online.`;
                }
            });
        }
    });
}

/* -- USER LOGIN -- */
module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null) {
            // email is not registered
            return 'Email is not registered.';
        } else {
            // email is registered and found
            const match = bcrypt.compareSync(reqBody.password, result.password);
            if(!match) {
                // failed
                return 'Error occurred - failed to create token';
            } else {
                // successful
                return {access: auth.createToken(result)};
            }
        }
    });
}


/* -- USER DETAILS RETRIEVAL -- */
module.exports.getUser = (data) => {
    return User.findById(data.userId).then(result => {
        if(data.userToken !== data.userId) {
            // user not found
            return `User does not exist.`
        }
        // hides password
        result.password = "";
        return result;
    })
}

/* -- SET USER TO ADMIN -- */
// couldn't get it to work because of little understanding and time crunch.
