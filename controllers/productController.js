/* -- SETUP -- */
const Product = require("../models/Product.js");
const User = require("../models/User.js")

// --------------------------------------------------- //
/* -- PRODUCT POSTING -- */
module.exports.addProduct = async (data) => {
	if(data.isAdmin) {
		let newProduct = await new Product({
			name: data.product.name, 
			price: data.product.price, 
			description: data.product.description
		});
		return newProduct.save().then((product, error) => {
			if(error) {
				return 'Product failed to post.'
			} else {
				return `Product (${newProduct.name}) was posted and is now available.`
			}
		});
	} else {
		return 'Only admins are capable of posting products.'
	}
}

/* -- PRODUCT RETRIEVAL -- */
module.exports.getAllProducts = () => {
	return Product.find({ isActive: true }).then(result => {
		return result
	})
}

/* -- PRODUCT SPECIFIED RETRIEVAL -- */
module.exports.getProduct = (productId) => {
	return Product.findById(productId.productId)
		.then((result, error) => {
			if(error) {
				return 'Error occurred - failed to retrieve product.'
			} else {
				return result
			}
		})
}

/* -- PRODUCT UPDATE -- */
module.exports.updateProduct = async (data) => {
	// if admin
	if(!data.isAdmin) return { error: "Unauthorized" };
	// something
	try {
		const product = await Product.findById(data.id.productId)
		if(!product) return { error: "Product not found"}

		const existingProduct = await Product.findOne({
			name: data.product.name,
			_id: { $ne: data.id.productId }
		}).collation({ locale: 'en', strength: 2 })

		// checks duplicate name
		if(existingProduct) return { message: "Product with that name already exists"}

		await Product.findByIdAndUpdate(data.id.productId, data.product)
		return { message: "Product updated"}
	} catch (err) {
		return false
	}
}

//ARCHIVE A PRODUCT 
